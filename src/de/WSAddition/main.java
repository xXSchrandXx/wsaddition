package de.WSAddition;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.UnknownDependencyException;
import org.bukkit.plugin.java.JavaPlugin;

import de.WSAddition.API.Config;
import de.WSAddition.Essentials.home;
import de.WSAddition.Essentials.spawn;
import de.WSAddition.Essentials.warp;
import de.WSAddition.WSAddition.worldsystemaddition;

public class main extends JavaPlugin {
// Variablen
  public Config config;
  public Config messages;
  private static main instance;
  public static boolean EssentialsHome = true;
  public static boolean EssentialsSpawn = true;
  public static boolean EssentialsWarp = true;
// Methoden
  public static main getInstance() {
    return instance;
  }
  public static String Loop(String Message){
    return ChatColor.translateAlternateColorCodes('&', Message).replace('\\', '\n');
  }
  @Override
  public void onEnable() {
    instance = this;
    if (Bukkit.getPluginManager().getPlugin("WorldSystem") == null) {
      try {
        Bukkit.getPluginManager().loadPlugin(new File(getDataFolder().getParentFile(), "WorldSystem.jar"));
      } catch (UnknownDependencyException | InvalidPluginException | InvalidDescriptionException e) {
        getLogger().log(Level.WARNING, "Plugin can't load without WorldSystem!");
        getLogger().log(Level.WARNING, "Download: https://www.spigotmc.org/resources/worldsystem-%E2%97%8F-the-one-world-per-player-solution-%E2%97%8F-1-8-1-12-2.49756/");
        return;
      }
    }
    config = new Config(this, "config.yml");
    config.reload();
    config.get().options().copyDefaults(true);
    config.get().options().header("# This plugin is in developement!");
    config.get().addDefault("Permission.Command.Main", "wsaddition.command.main");
    config.get().addDefault("Permission.Command.Info", "wsaddition.command.info");
    config.get().addDefault("Essentials.Home.AutoLoadWorld", true);
    config.get().addDefault("Essentials.Warp.AutoLoadWorld", true);
    config.get().addDefault("Essentials.Spawn.SetEssentialsSpawn", true);
    config.save();
    messages = new Config(this, "messages.yml");
    messages.reload();
    messages.get().options().copyDefaults(true);
    messages.get().options().header("# This plugin is in developement!");
    messages.get().addDefault("Prefix", "&8[&6WSAddition&8] &7");
    messages.get().addDefault("OnlyConsole", "&4ERROR&7, Only console can perform this command.");
    messages.get().addDefault("PluginInformation", "Plugininformation:");
    messages.get().addDefault("NoPermission", "&4You don't have enough permissions.");
    messages.get().addDefault("Essentials.Home.WorldNotLoad", "&4ERROR&7, World couldn't get loaded.");
    messages.get().addDefault("Essentials.Warp.PlayerNotOnline", "Player &e%input%&7 is not online!");
    messages.get().addDefault("Essentials.Warp.WorldNotLoad", "&4ERROR&7, World couldn't get loaded.");
    messages.save();
    config.reload();
    messages.reload();
    EssentialsHome = config.get().getBoolean("Essentials.Home.AutoLoadWorld");
    EssentialsSpawn = config.get().getBoolean("Essentials.Spawn.SetEssentialsSpawn");
    EssentialsWarp = config.get().getBoolean("Essentials.Warp.AutoLoadWorld");
//  Lade Kommandos
    getCommand("worldsystemaddition").setExecutor(new worldsystemaddition());
    getLogger().log(Level.INFO, "Registered commands.");
//  Lade Events
    if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
      getLogger().log(Level.INFO, "Hooked into Essentials.");
      if (EssentialsHome)
        Bukkit.getPluginManager().registerEvents(new home(), this);
      if (EssentialsWarp)
        Bukkit.getPluginManager().registerEvents(new warp(), this);
      if (Bukkit.getPluginManager().getPlugin("EssentialsSpawn") != null) {
        getLogger().log(Level.INFO, "Hooked into EssentialsSpawn");
       if (EssentialsSpawn)
        Bukkit.getPluginManager().registerEvents(new spawn(), this);
      }
      else {
        EssentialsSpawn = false;
      }
    }
    else {
      EssentialsHome = false;
      EssentialsSpawn = false;
      EssentialsWarp = false;
    }
    getLogger().log(Level.INFO, "Registered events.");
  }
  @Override
  public void onDisable() {
    if (Bukkit.getPluginManager().getPlugin("WorldSystem") == null) {
      getLogger().log(Level.WARNING, "Plugin can't work without WorldSystem!");
      getLogger().log(Level.WARNING, "Download: https://www.spigotmc.org/resources/worldsystem-%E2%97%8F-the-one-world-per-player-solution-%E2%97%8F-1-8-1-12-2.49756/");
      return;
    }
    if (EssentialsHome) {
      HandlerList.unregisterAll(new home());
      getLogger().log(Level.INFO, "Removed EssentialsHomeAddition");
    }
    if (EssentialsWarp) {
      HandlerList.unregisterAll(new warp());
      getLogger().log(Level.INFO, "Removed EssentialsWarpAddition");
    }
    if (EssentialsSpawn) {
      HandlerList.unregisterAll(new spawn());
      getLogger().log(Level.INFO, "Removed EssentialsSpawnAddition");
    }
  }
}
