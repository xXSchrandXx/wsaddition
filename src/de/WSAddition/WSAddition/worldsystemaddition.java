package de.WSAddition.WSAddition;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.WSAddition.main;

public class worldsystemaddition implements CommandExecutor {
  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    boolean perminfo = false;
    boolean permmain = true;
    boolean permreload = false;
    if (sender instanceof Player) {
      if (sender.hasPermission(main.getInstance().config.get().getString("Permission.Command.Info")))
        perminfo = true;
      if (sender.hasPermission(main.getInstance().config.get().getString("Permission.Command.Main")))
        permmain = false;
    }
    if (sender instanceof ConsoleCommandSender){
      perminfo = true;
      permmain = false;
      permreload = true;
    }
    if (args.length != 0) {
      if (args[0].equalsIgnoreCase("reload")) {
        if (permreload) {
          main.getInstance().onDisable();
          main.EssentialsHome = main.getInstance().config.get().getBoolean("Essentials.Home.AutoLoadWorld");
          main.EssentialsSpawn = main.getInstance().config.get().getBoolean("Essentials.Spawn.SetEssentialsSpawn");
          main.EssentialsWarp = main.getInstance().config.get().getBoolean("Essentials.Warp.AutoLoadWorld");
          main.getInstance().onEnable();
          return true;
        }
        else {
          sender.sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("OnlyConsole")));
          return true;
        }
      }
      else if (args[0].equalsIgnoreCase("info")) {
        if (perminfo) {
          sender.sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("PluginInformation")));
          sender.sendMessage(main.Loop("&7") + main.getInstance().getDescription().getName() + " " + main.getInstance().getDescription().getVersion() + " by " + main.getInstance().getDescription().getAuthors());
         if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
            sender.sendMessage(main.Loop("&7Essentials: &aEnabled"));
            sender.sendMessage(main.Loop("&7    ListenHomes: &e") + main.EssentialsHome);
            sender.sendMessage(main.Loop("&7    ListenWarps: &e") + main.EssentialsWarp);
          }
          else {
            sender.sendMessage(main.Loop("&7Essentials: &cDisabled"));
          }
          if (Bukkit.getPluginManager().getPlugin("EssentialsSpawn") != null) {
            sender.sendMessage(main.Loop("&7EssentialsSpawn: &aEnabled"));
            sender.sendMessage(main.Loop("&7    ChangeSpawn: &e") + main.EssentialsSpawn);
          }
          else {
            sender.sendMessage(main.Loop("&7EssentialsSpawn: &cDisabled"));
          }
          return true;
        }
        else {
          sender.sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("NoPermission")));
          return true;
        }
      }
    }
    if (permmain) {
      sender.sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("NoPermission")));
      return true;
    }
    sender.sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + "&e" + main.getInstance().getDescription().getName() + " " + main.getInstance().getDescription().getVersion()) + " by " + main.getInstance().getDescription().getAuthors());
    return true;  
  }
}
