package de.WSAddition.Essentials;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.earth2me.essentials.Essentials;

import de.WSAddition.main;
import de.WSAddition.API.Config;
import de.butzlabben.world.WorldSystem;
import de.butzlabben.world.wrapper.SystemWorld;

public class warp implements Listener {
  private Essentials ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");
  @EventHandler(priority=EventPriority.HIGHEST)
  public void onCommand(PlayerCommandPreprocessEvent e) {
    String[] args = e.getMessage().split(" ");
    Object world = null;
    Player p = e.getPlayer();
    if (args[0].equalsIgnoreCase("/warp")) {
      if (args.length == 2) {
        if (ess.getWarps().getList() != null) {
          for (String warp : ess.getWarps().getList()) {
            if (warp.equalsIgnoreCase(args[1])) {
              Config warpcgf = new Config(ess, "warps/" + args[1] + ".yml");
              world = warpcgf.get().getString("world");
            }
          }
        }
      }
      else if (args.length == 3) {
        if (ess.getWarps().getList() != null) {
          for (String warp : ess.getWarps().getList()) {
            if (warp.equalsIgnoreCase(args[1])) {
              Config warpcgf = new Config(ess, "warps/" + args[1] + ".yml");
              world = warpcgf.get().getString("world");
              if (Bukkit.getPlayer(args[2]) != null) {
                p = Bukkit.getPlayer(args[2]);
              }
              else {
                e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Warp.PlayerNotOnline").replace("%input%", args[2])));
                e.setCancelled(true);
              }
            }
          }
        }
      }
      if (world != null) {
        WorldSystem ws = (WorldSystem) Bukkit.getPluginManager().getPlugin("WorldSystem");
        Config dconfig = new Config(ws, "dependence.yml");
        for (Object uuid : dconfig.get().getConfigurationSection("Dependences").getKeys(false)) {
          Object id = dconfig.get().get("Dependences." + uuid + ".ID");
          Object worldname = "ID" + id + "-" + uuid;
          if (world.equals(worldname)) {
            SystemWorld sw = SystemWorld.getSystemWorld(world.toString());
            if (sw != null) {
              if (!sw.isLoaded()) {
                sw.load(p);
              }
            }
          }
        }
      }
      else {
        e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Warp.WorldNotLoad")));
      }
    }
  }
}
