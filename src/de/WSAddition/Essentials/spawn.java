package de.WSAddition.Essentials;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.spawn.EssentialsSpawn;

import de.WSAddition.main;
import de.butzlabben.world.config.MessageConfig;
import de.butzlabben.world.config.PluginConfig;
import de.butzlabben.world.wrapper.SystemWorld;
import de.butzlabben.world.wrapper.WorldPlayer;

public class spawn implements Listener {
  private Essentials ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");
  private EssentialsSpawn essspawn = (EssentialsSpawn) Bukkit.getPluginManager().getPlugin("EssentialsSpawn");
  @EventHandler
  public void onLeave(PlayerCommandPreprocessEvent e) {
    if (e.getMessage().toLowerCase().startsWith("/ws leave")) {
      e.setCancelled(true);
      Player p = e.getPlayer();
      String worldname = p.getWorld().getName();
      WorldPlayer wp = new WorldPlayer(p, worldname);
      if (wp.isOnSystemWorld()) {
        if (essspawn.getSpawn(ess.getUser(p).getGroup()) != null) {
          World w = Bukkit.getWorld(p.getWorld().getName());
          p.teleport(essspawn.getSpawn(ess.getUser(p).getGroup()));
          SystemWorld.tryUnloadLater(w);
        }
        else {
          p.sendMessage(PluginConfig.getPrefix() + main.Loop("&cThe spawn is not properly set"));
        }
      }
      else {
        p.sendMessage(MessageConfig.getNotOnWorld());
      }
    }
  }
}
