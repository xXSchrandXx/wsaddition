package de.WSAddition.Essentials;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.earth2me.essentials.Essentials;

import de.WSAddition.main;
import de.WSAddition.API.Config;
import de.butzlabben.world.WorldSystem;
import de.butzlabben.world.wrapper.SystemWorld;

public class home implements Listener {
  private Essentials ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");
  @SuppressWarnings("deprecation")
  @EventHandler(priority=EventPriority.HIGHEST)
  public void onCommand(PlayerCommandPreprocessEvent e) {
    String[] args = e.getMessage().split(" ");
    if (args[0].equalsIgnoreCase("/home")) {
      Config essplayerconfig = new Config(ess, "userdata/" + e.getPlayer().getUniqueId() + ".yml");
      Object world = null;
      WorldSystem ws = (WorldSystem) Bukkit.getPluginManager().getPlugin("WorldSystem");
      Config dconfig = new Config(ws, "dependence.yml");
      if (args.length == 1) {
        if (ess.getUser(e.getPlayer()).getHomes() != null) {
          if (!ess.getUser(e.getPlayer()).getHomes().isEmpty()) {
            List<String> homes = ess.getUser(e.getPlayer()).getHomes();
            Object name = homes.get(0);
            for (Object uuid : dconfig.get().getConfigurationSection("Dependences").getKeys(false)) {
              Object id = dconfig.get().get("Dependences." + uuid + ".ID");
              Object worldname = "ID" + id + "-" + uuid;
              for (Object homeworld : essplayerconfig.get().getConfigurationSection("homes").getKeys(false)) {
                if (worldname == essplayerconfig.get().get("homes." + homeworld + ".world")) {
                  name = worldname;
                }
              }
            }
            try {
              world = essplayerconfig.get().get("homes." + name + ".world");
            } catch (Exception e1) {
              e1.printStackTrace();
              e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Home.WorldNotLoad")));
              e.setCancelled(true);
            }
          }
          else {
            e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Home.WorldNotLoad")));
            e.setCancelled(true);
          }
        }
        else {
          e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Home.WorldNotLoad")));
          e.setCancelled(true);
        }
      }
      else {
        String[] args2 = args[1].split(":");
        if (args2.length == 1) {
          try {
            world = essplayerconfig.get().get("homes." + args[1]+ ".world");
          } catch (Exception e1) {
            e1.printStackTrace();
            e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Home.WorldNotLoad")));
            e.setCancelled(true);
          }
        }
        else {
          essplayerconfig = new Config(ess, "userdata/" + Bukkit.getOfflinePlayer(args2[0]).getUniqueId() + ".yml");
          try {
            world = essplayerconfig.get().get("homes." + args2[1] + ".world");
          } catch (Exception e1) {
            e1.printStackTrace();
            e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Home.WorldNotLoad")));
            e.setCancelled(true);
          }
        }
      }
      if (world != null) {
        for (Object uuid : dconfig.get().getConfigurationSection("Dependences").getKeys(false)) {
          Object id = dconfig.get().get("Dependences." + uuid + ".ID");
          Object worldname = "ID" + id + "-" + uuid;
          if (world.equals(worldname)) {
            SystemWorld sw = SystemWorld.getSystemWorld(world.toString());
            if (sw != null) {
              if (!sw.isLoaded()) {
                sw.load(e.getPlayer());
              }
            }
          }
        }
      }
      else {
        e.getPlayer().sendMessage(main.Loop(main.getInstance().messages.get().getString("Prefix") + main.getInstance().messages.get().getString("Essentials.Home.WorldNotLoad")));
      }
    }
  }
}
